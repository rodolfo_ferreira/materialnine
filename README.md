# README #

This App was created to insert the 9th digit in the Brazilian numbers of your contacts, it was developed because in 2015 the ANATEL started to implant a 9th digit in all mobile numbers, in order to avoid the mechanical task of adding the number '9' in each contact I created this App.

### What is this repository for? ###

* This repository was created as backup and version control

### How do I get set up? ###

* Import in your IDE (Android Studio preferenced), then run.

### Contribution guidelines ###

* Developed, Build and Tested by Rodolfo Ferreira

### Who do I talk to? ###

* For further info contact me: RodolfoAndreFerreira@gmail.com