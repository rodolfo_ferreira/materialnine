package com.dolphs.materialblank;

import android.content.Context;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.dolphs.materialdesign.FloatingActionButton;

/**
 * Created by Rodolfo on 12/06/2015.
 */
public class DolphsUtil {

    @Deprecated
    public static void fabHideEffect(Context context, final FloatingActionButton fab) {
        Animation anim = AnimationUtils.loadAnimation(context, R.anim.zoom_out);
        fab.setAnimation(anim);

        fab.startAnimation(anim);
        fab.setVisibility(View.GONE);
        fab.setOnClickListener(null);
    }

    @Deprecated
    public static void fabShowEffect(Context context, final FloatingActionButton fab) {
        Animation anim = AnimationUtils.loadAnimation(context, R.anim.zoom_in);
        fab.setAnimation(anim);

        fab.startAnimation(anim);
        fab.setVisibility(View.VISIBLE);
    }

    @Deprecated
    public static void fabHideEffect(Context context, final FloatingActionButton fab, long duration) {
        Animation anim = AnimationUtils.loadAnimation(context, R.anim.zoom_out);
        anim.setDuration(duration);
        fab.setAnimation(anim);

        fab.startAnimation(anim);
        fab.setVisibility(View.GONE);
        fab.setOnClickListener(null);
    }

    @Deprecated
    public static void fabShowEffect(Context context, final FloatingActionButton fab, long duration) {
        Animation anim = AnimationUtils.loadAnimation(context, R.anim.zoom_in);
        anim.setDuration(duration);
        fab.setAnimation(anim);

        fab.startAnimation(anim);
        fab.setVisibility(View.VISIBLE);
    }

    public static void animate(Context context, View view, int animatonResId, int visibility) {
        Animation anim = AnimationUtils.loadAnimation(context, animatonResId);
        view.setAnimation(anim);

        view.startAnimation(anim);
        view.setVisibility(visibility);

    }

    public static void animate(Context context, View view, int animatonResId, int visibility, long duration) {
        Animation anim = AnimationUtils.loadAnimation(context, animatonResId);
        anim.setDuration(duration);
        view.setAnimation(anim);

        view.startAnimation(anim);
        view.setVisibility(visibility);

    }
}
