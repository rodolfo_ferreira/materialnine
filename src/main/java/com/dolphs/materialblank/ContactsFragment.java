package com.dolphs.materialblank;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.LayerDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import com.dolphs.materialdesign.FloatingActionButton;
import com.dolphs.materialdesign.SlidingTabLayout;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ContactsFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ContactsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ContactsFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private Toolbar mToolbar;
    private ImageButton mImageButton;
    private View view;
    private ArrayList<Contact> mContacts;
    private OnPageScrolled mCallback;
    private OnPageScrolled mCallback2;

    private SlidingTabLayout mTabs;
    private ViewPager mPager;
    private ViewPagerAdapter mAdapter;
    private int numOfTabs = 2;
    private FloatingActionButton mFab;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public ContactsFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ContactsFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ContactsFragment newInstance(String param1, String param2) {
        ContactsFragment fragment = new ContactsFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ContactsManager contactsManager = ContactsManager.getInstance();
        mContacts = contactsManager.getContacts();
        View rootView = inflater.inflate(R.layout.fragment_contacts, container, false);
        CharSequence Titles[] = {getResources().getString(R.string.title_tab_1), getResources().getString(R.string.title_tab_2)};
        mToolbar = (Toolbar) rootView.findViewById(R.id.contacts_toolbar);
        mToolbar.setTitle(getString(R.string.app_name));
        mToolbar.inflateMenu(R.menu.toolbar_menu);
        mAdapter = new ViewPagerAdapter(getActivity().getSupportFragmentManager(), Titles, numOfTabs);
        mPager = (ViewPager) rootView.findViewById(R.id.pager);
        mPager.setAdapter(mAdapter);
        mPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }


            @Override
            public void onPageSelected(int position) {
                OnScroll(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        mTabs = (SlidingTabLayout) rootView.findViewById(R.id.tabs);
        mTabs.setLightTheme(true);
        mTabs.setDistributed(true);
        mTabs.setCustomTabColorizer(new SlidingTabLayout.TabColorizer() {
            @Override
            public int getIndicatorColor(int position) {
                TypedValue typedValue = new TypedValue();
                Resources.Theme theme = getActivity().getTheme();
                theme.resolveAttribute(R.attr.colorPrimaryDark, typedValue, true);
                int color = typedValue.data;
                return color;
            }

            @Override
            public int getDividerColor(int position) {
                return position;
            }
        });
        mTabs.setViewPager(mPager);

        mCallback = (OnPageScrolled) mAdapter.getItem(0);
        mCallback2 = (OnPageScrolled) mAdapter.getItem(1);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            mFab = (FloatingActionButton) rootView.findViewById(R.id.fab);
            mFab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int length = mContacts.size();
                    ContentUpdate contentUpdate = new ContentUpdate(getActivity());
                    contentUpdate.execute();
                    if (mContacts == null || mContacts.size() == 0)
                        Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.missing_contacts), Toast.LENGTH_SHORT).show();
                    else {
                        mAdapter.notifyDataSetChanged();
                    }
                }
            });
        } else {
            final ImageView imageView = (ImageView) rootView.findViewById(R.id.fab);
            LayerDrawable bgDrawable = (LayerDrawable) imageView.getBackground();
            final GradientDrawable shape = (GradientDrawable) bgDrawable
                    .findDrawableByLayerId(R.id.shape_id);

            imageView.setImageResource(R.drawable.ic_sync_white_48dp);
            imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int length = mContacts.size();
                    for (Contact contact : mContacts) {
                        //editContact(contact);
                    }
                    if (mContacts == null || mContacts.size() == 0)
                        Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.missing_contacts), Toast.LENGTH_SHORT).show();
                    else {
                        OnScroll(1);
                        OnScroll(2);
                    }


                }
            });
        }


        return rootView;
    }

    private void OnScroll(int position) {
        if (mCallback != null) {
            mCallback.OnPageScrolled(position, getActivity());
        }
        if (mCallback2 != null) {
            mCallback2.OnPageScrolled(position, getActivity());
        }
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        DolphsUtil.animate(getActivity(), mTabs, R.anim.fall_in, View.VISIBLE);
        DolphsUtil.animate(getActivity(), mFab, R.anim.zoom_in, View.VISIBLE);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }

    public interface OnPageScrolled {
        public void OnPageScrolled(int position, Context context);
    }


}
