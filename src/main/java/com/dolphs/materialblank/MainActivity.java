package com.dolphs.materialblank;

import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;

import java.util.ArrayList;


public class MainActivity extends AppCompatActivity implements
        ContactsFragment.OnFragmentInteractionListener,
        WelcomeFragment.OnFragmentInteractionListener,
        ContactsAllFragment.OnFragmentInteractionListener, ContactsOutdatedFragment.OnFragmentInteractionListener {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //ContactsFragment frag = new ContactsFragment();
        WelcomeFragment mWelcomeFrag = new WelcomeFragment();
        getFragmentManager().beginTransaction()
                .add(R.id.fragment_container, mWelcomeFrag).commit();

        //FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        //transaction.replace(R.id.fragment_container, frag);
        //transaction.addToBackStack(null);
        //transaction.commit();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.toolbar_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onFragmentInteraction() {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        ContactsFragment mContactsFrag = new ContactsFragment();
        // Replace whatever is in the fragment_container view with this fragment,
        // and add the transaction to the back stack
        transaction.replace(R.id.fragment_container, mContactsFrag);

        // Commit the transaction
        transaction.commit();
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}
