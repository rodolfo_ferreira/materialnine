package com.dolphs.materialblank;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import java.util.ArrayList;

/**
 * Created by Rodolfo on 14/06/2015.
 */
public class MyOnMenuItemClickListener implements Toolbar.OnMenuItemClickListener {
    private Context mContext;
    private RecyclerView.Adapter mAdapter;

    public MyOnMenuItemClickListener(Context context, RecyclerView.Adapter adapter) {
        mContext = context;
        mAdapter = adapter;

    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_check_all:
                boolean someSelected = false;
                ContactsManager contactsManager = ContactsManager.getInstance();
                ArrayList<Contact> contacts = contactsManager.getContacts();
                for (int i = 0; i < contacts.size(); i++) {
                    if (contacts.get(i).isSelected())
                        someSelected = true;
                }

                contactsManager.setAllSelection(!someSelected);


                mAdapter.notifyDataSetChanged();
                return true;
        }

        return false;
    }
}
