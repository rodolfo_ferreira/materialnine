package com.dolphs.materialblank;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Rodolfo on 30/05/2015.
 */
public class ContactsAdapter extends RecyclerView.Adapter<ContactsAdapter.ViewHolder> {
    private ArrayList<Contact> mContacts;
    private Context mContext;
    private int lastPosition = -1;


    // Provide a suitable constructor (depends on the kind of dataset)
    public ContactsAdapter(ArrayList<Contact> contacts, Context context) {
        if (context == null)
            throw new NullPointerException();
        mContacts = contacts;
        mContext = context;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public ContactsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                         int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.contact_item, parent, false);
        // set the view's size, margins, paddings and layout parameters

        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        final Contact contact = mContacts.get(position);
        holder.mTextViewName.setText(contact.getFirstName());
        if (contact.getNewNumber() == null)
            holder.mTextViewNewNumber.setText(mContext.getResources().getString(R.string.updated));
        else
            holder.mTextViewNewNumber.setText(contact.getNewNumber());
        holder.mTextViewNumber.setText(contact.getNumber());
        final View view = holder.mTextViewNewNumber;
        holder.mCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    contact.setSelected(true);
                    DolphsUtil.animate(mContext, view, R.anim.fall_in, View.VISIBLE);

                } else {
                    contact.setSelected(false);
                    DolphsUtil.animate(mContext, view, R.anim.fall_out, View.INVISIBLE);
                }
            }
        });
        holder.mCheckBox.setChecked(contact.isSelected());
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mContacts.size();
    }

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        // each data item is just a string in this case
        public TextView mTextViewName;
        public TextView mTextViewNumber;
        public TextView mTextViewNewNumber;
        public CheckBox mCheckBox;
        public RelativeLayout mContactItem;

        public ViewHolder(View v) {
            super(v);
            if (v == null)
                throw new NullPointerException();
            v.setOnClickListener(this);
            mTextViewName = (TextView) v.findViewById(R.id.contactName);
            mTextViewNumber = (TextView) v.findViewById(R.id.contactNumber);
            mTextViewNewNumber = (TextView) v.findViewById(R.id.contactNewNumber);
            mTextViewNewNumber.setVisibility(View.INVISIBLE);
            mCheckBox = (CheckBox) v.findViewById(R.id.checkBox);
            mContactItem = (RelativeLayout) v.findViewById(R.id.contactItem);

        }

        @Override
        public void onClick(View v) {
            if (mCheckBox.isChecked())
                mCheckBox.setChecked(false);
            else
                mCheckBox.setChecked(true);
        }
    }
}
