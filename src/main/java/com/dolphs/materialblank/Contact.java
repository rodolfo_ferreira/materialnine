package com.dolphs.materialblank;

/**
 * Created by Rodolfo on 31/05/2015.
 */
public class Contact {

    private boolean selected;

    private boolean hasBeenChanged;

    private int id;

    private String firstName;

    private String number;

    private String newNumber;

    public Contact(String id, String firstName, String number) {
        this.id = Integer.valueOf(id);
        this.firstName = firstName;
        this.number = number;
        this.newNumber = updateNumber();
        this.selected = false;
        this.hasBeenChanged = false;
    }

    public Contact(String contactId, String name, String number, String newNumber, String isSelected, String hasBeenChanged) {
        this.id = Integer.valueOf(contactId);
        this.firstName = name;
        this.number = number;
        this.newNumber = newNumber;
        this.selected = Boolean.valueOf(isSelected);
        this.hasBeenChanged = Boolean.valueOf(hasBeenChanged);
    }

    private String updateNumber() {
        String result = getNumber().replaceAll("[\\ \\*\\-N\\(\\)\\/\\\\\\.\\#]+", "");
        int length = result.length();
        if (length == 9 || result.startsWith("0800")) {
            result = null;
        } else if (result.matches("[789][0-9]{7}")) {
            result = 9 + result.substring(0, 4) + "-" + result.substring(4, 8);
        } else if (result.matches("[0]*[0-9]{2}[789][0-9]{7}")) {
            if (result.startsWith("0")) {
                result = result.substring(0, 3) + " " + 9 + result.substring(3, 7) + "-" +
                        result.substring(7, 11);
            } else {
                result = result.substring(0, 2) + " " + 9 + result.substring(2, 6) + "-" +
                        result.substring(6, 10);
            }
        } else if (result.matches("^0[0-9]{2}[0-9]{2}[789][0-9]{7}")) {
            result = result.substring(0, 3) + " " + result.substring(3, 5) + " " + "9" +
                    result.substring(5, 9) + "-" + result.substring(9, 13);
        } else if (result.matches("^\\+[0-9]*[0-9]{2}[0-9]{2}[789][0-9]{7}")) {
            result = result.substring(0, 3) + " " + result.substring(3, 5) + " " + "9" +
                    result.substring(5, 9) + "-" + result.substring(9, 13);
        } else {
            result = null;
        }

        return result;
    }

    public String getNumber() {
        return number;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getNewNumber() {
        return newNumber;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public boolean hasBeenChanged() {
        return hasBeenChanged;
    }

    public int getId() {
        return id;
    }
}
