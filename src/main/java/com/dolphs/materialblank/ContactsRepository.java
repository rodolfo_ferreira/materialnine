package com.dolphs.materialblank;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

/**
 * Created by Rodolfo on 16/06/2015.
 */
public class ContactsRepository {

    public static final String COLUMN_ID = "CONTACT_ID";
    public static final String COLUMN_NAME = "NAME";
    public static final String COLUMN_NUMBER = "NUMBER";
    public static final String COLUMN_NEW_NUMBER = "NEW_NUMBER";
    public static final String COLUMN_IS_SELECTED = "IS_SELECTED";
    public static final String COLUMN_HB_CHANGED = "HAS_BEEN_CHANGED";
    private static ContactsRepository instance;
    private SQLiteDatabase dataBase = null;

    private ContactsRepository(Context context) {
        TablesManager persistenceHelper = TablesManager.getInstance(context);
        dataBase = persistenceHelper.getWritableDatabase();
    }

    public static ContactsRepository getInstance(Context context) {
        if (instance == null)
            instance = new ContactsRepository(context);
        return instance;
    }

    public void add(Contact contact) {
        ContentValues[] values = generateContentValues(contact);
        dataBase.insertWithOnConflict(TablesManager.TABLE_CONTACTS, null, values[0], SQLiteDatabase.CONFLICT_IGNORE);
        dataBase.insertWithOnConflict(TablesManager.TABLE_NUMBERS, null, values[1], SQLiteDatabase.CONFLICT_IGNORE);
    }

    public Contact getContactByName(String title) {
        Contact result = null;
        String str = COLUMN_NAME + " = ?";
        Cursor contact_cursor = dataBase.query(TablesManager.TABLE_CONTACTS, null, str,
                new String[]{title}, null, null, null);
        if (contact_cursor.getCount() > 0) {
            contact_cursor.moveToFirst();
            int indexID = contact_cursor.getColumnIndex(COLUMN_ID);
            int indexName = contact_cursor.getColumnIndex(COLUMN_NAME);
            String contactId = contact_cursor.getString(indexID);
            String name = contact_cursor.getString(indexName);
            String search = COLUMN_ID + " = ?";
            Cursor number_cursor = dataBase.query(TablesManager.TABLE_CONTACTS, null, str,
                    new String[]{contactId}, null, null, null);
            if (number_cursor.getCount() > 0) {
                int indexNumber = number_cursor.getColumnIndex(COLUMN_NUMBER);
                int indexNewNumber = number_cursor.getColumnIndex(COLUMN_NEW_NUMBER);
                int indexIsSelected = number_cursor.getColumnIndex(COLUMN_IS_SELECTED);
                int indexChanged = number_cursor.getColumnIndex(COLUMN_HB_CHANGED);
                String number = number_cursor.getString(indexNumber);
                String newNumber = number_cursor.getString(indexNewNumber);
                String inSelected = number_cursor.getString(indexIsSelected);
                String hasBeenChanged = number_cursor.getString(indexChanged);
                result = new Contact(contactId, name, number, newNumber, inSelected, hasBeenChanged);
            }
            if (!number_cursor.isClosed())
                number_cursor.close();
        }
        if (!contact_cursor.isClosed())
            contact_cursor.close();
        return result;
    }

    public ArrayList<Contact> getAll() {
        String queryReturnAllContacts = "SELECT * FROM " + TablesManager.TABLE_CONTACTS + " ORDER BY "
                + COLUMN_NAME + " ASC";
        String queryReturnAllNumbers = "SELECT * FROM " + TablesManager.TABLE_NUMBERS + " ORDER BY "
                + COLUMN_ID + " ASC";
        Cursor contact_cursor = dataBase.rawQuery(queryReturnAllContacts, null);
        Cursor number_cursor = dataBase.rawQuery(queryReturnAllNumbers, null);
        ArrayList<Contact> contacts = buildContacts(contact_cursor, number_cursor);
        if (!contact_cursor.isClosed())
            contact_cursor.close();
        if (!number_cursor.isClosed())
            number_cursor.close();
        return contacts;
    }

    public void edit(Contact contact) {
        ContentValues[] valores = generateContentValues(contact);
        String[] valoresParaSubstituir = {String.valueOf(contact.getId())};
        dataBase.update(TablesManager.TABLE_NUMBERS, valores[1], COLUMN_ID + " = ?",
                valoresParaSubstituir);
    }

    public void closeConection() {
        if (dataBase != null && dataBase.isOpen())
            dataBase.close();
    }

    private ArrayList<Contact> buildContacts(Cursor contact_cursor, Cursor number_cursor) {
        ArrayList<Contact> result = new ArrayList<Contact>();
        if (contact_cursor != null && number_cursor != null)
            try {
                if (contact_cursor.moveToFirst()) {
                    do {
                        int indexID = contact_cursor.getColumnIndex(COLUMN_ID);
                        int indexName = contact_cursor.getColumnIndex(COLUMN_NAME);
                        String contactId = contact_cursor.getString(indexID);
                        String name = contact_cursor.getString(indexName);
                        if (number_cursor.moveToFirst()) {
                            do {
                                int indexNID = number_cursor.getColumnIndex(COLUMN_ID);
                                int indexNumber = number_cursor.getColumnIndex(COLUMN_NUMBER);
                                int indexNewNumber = number_cursor.getColumnIndex(COLUMN_NEW_NUMBER);
                                int indexIsSelected = number_cursor.getColumnIndex(COLUMN_IS_SELECTED);
                                int indexChanged = number_cursor.getColumnIndex(COLUMN_HB_CHANGED);
                                String number = number_cursor.getString(indexNumber);
                                String newNumber = number_cursor.getString(indexNewNumber);
                                String inSelected = number_cursor.getString(indexIsSelected);
                                String hasBeenChanged = number_cursor.getString(indexChanged);
                                String numberId = contact_cursor.getString(indexID);
                                if (contactId.equals(numberId)) {
                                    Contact contact = new Contact(contactId, name, number, newNumber, inSelected, hasBeenChanged);
                                    result.add(contact);
                                }
                            } while (number_cursor.moveToNext());
                        }
                    } while (contact_cursor.moveToNext());
                }
            } finally {
                contact_cursor.close();
                number_cursor.close();
            }
        return result;
    }

    private ContentValues[] generateContentValues(Contact contact) {
        ContentValues[] values = new ContentValues[2];
        ContentValues contactTable = new ContentValues();
        ContentValues numberTable = new ContentValues();
        contactTable.put(COLUMN_ID, contact.getId());
        contactTable.put(COLUMN_NAME, contact.getFirstName());
        numberTable.put(COLUMN_ID, contact.getId());
        numberTable.put(COLUMN_NUMBER, contact.getNumber());
        numberTable.put(COLUMN_NEW_NUMBER, contact.getNewNumber());
        numberTable.put(COLUMN_IS_SELECTED, contact.isSelected());
        numberTable.put(COLUMN_HB_CHANGED, contact.hasBeenChanged());
        values[0] = contactTable;
        values[1] = numberTable;
        return values;
    }
}