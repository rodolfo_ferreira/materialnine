package com.dolphs.materialblank;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ContactsOutdatedFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ContactsOutdatedFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ContactsAllFragment extends Fragment implements ContactsFragment.OnPageScrolled {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private RecyclerView mRecyclerView;
    private RecyclerView.LayoutManager mLayoutManager;
    private ContactsAdapter mAdapter;
    private Toolbar mToolbar;

    private OnFragmentInteractionListener mListener;

    public ContactsAllFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment AllContacts.
     */
    // TODO: Rename and change types and number of parameters
    public static ContactsOutdatedFragment newInstance(String param1, String param2) {
        ContactsOutdatedFragment fragment = new ContactsOutdatedFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.v("MaterialLog", "onCreate");
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ContactsManager contactsManager = ContactsManager.getInstance();
        ArrayList<Contact> contacts = contactsManager.getContacts();
        mAdapter = new ContactsAdapter(contacts, getActivity());
        Log.v("MaterialLog", "onCreateView");
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_all_contacts, container, false);
        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.all_contacts_recycler_view);
        mLayoutManager = new LinearLayoutManager(getActivity());
        mLayoutManager.scrollToPosition(0);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setAdapter(mAdapter);
        mToolbar = (Toolbar) getActivity().findViewById(R.id.contacts_toolbar);
        MyOnMenuItemClickListener menuItemClickListener = new MyOnMenuItemClickListener(getActivity(), mAdapter);
        mToolbar.setOnMenuItemClickListener(menuItemClickListener);

        return rootView;
    }

    // TODO: Rename method, update argument and hook method into UI event

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        DolphsUtil.animate(getActivity(), mRecyclerView, R.anim.slide_in_right, View.VISIBLE);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void OnPageScrolled(final int position, Context context) {
        if (position == 0) {
            MainActivity activity = ((MainActivity) context);
            mToolbar = (Toolbar) activity.findViewById(R.id.contacts_toolbar);
            ContactsManager contactsManager = ContactsManager.getInstance();
            ArrayList<Contact> contacts = contactsManager.getContacts();
            mRecyclerView = (RecyclerView) activity.findViewById(R.id.all_contacts_recycler_view);
            mAdapter = (ContactsAdapter) mRecyclerView.getAdapter();
            mAdapter.notifyDataSetChanged();
            MyOnMenuItemClickListener menuItemClickListener = new MyOnMenuItemClickListener(context, mAdapter);
            mToolbar.setOnMenuItemClickListener(menuItemClickListener);

        }
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }

}
