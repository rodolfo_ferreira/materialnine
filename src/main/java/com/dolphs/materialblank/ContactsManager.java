package com.dolphs.materialblank;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.provider.ContactsContract;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/**
 * Created by Rodolfo on 19/06/2015.
 */
public class ContactsManager {

    private ArrayList<Contact> mContacts;

    private static ContactsManager instance;

    public static ContactsManager getInstance() {
        if (instance == null)
            instance = new ContactsManager();
        return instance;
    }

    public void buildContacts(Context context) {
        mContacts = new ArrayList<Contact>();
        ContentResolver cr = context.getContentResolver();
        Cursor cur = cr.query(ContactsContract.Contacts.CONTENT_URI,
                null, null, null, null);
        if (cur.getCount() > 0) {
            while (cur.moveToNext()) {
                String id = cur.getString(cur.getColumnIndex(ContactsContract.Contacts._ID));
                String name = cur.getString(cur.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME_PRIMARY));
                if (Integer.parseInt(cur.getString(
                        cur.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))) > 0) {
                    Cursor pCur = cr.query(
                            ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                            null,
                            ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?",
                            new String[]{id}, null);
                    while (pCur.moveToNext()) {
                        String phoneNo = pCur.getString(pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                        //String typeNo =  pCur.getString(pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE));
                        if (phoneNo != null) {
                            Contact contact = new Contact(id, name, phoneNo);
                            mContacts.add(contact);
                        }
                    }
                    pCur.close();
                }
            }
        }


        Collections.sort(mContacts, new Comparator<Contact>() {
            @Override
            public int compare(Contact contact1, Contact contact2) {

                return contact1.getFirstName().toLowerCase().compareTo(contact2.getFirstName().toLowerCase());
            }
        });
    }

    public ArrayList<Contact> getContacts() {
        return mContacts;
    }

    public void setAllSelection(boolean selection) {
        for (int i = 0; i < mContacts.size(); i++) {
            mContacts.get(i).setSelected(selection);
        }
    }
}
