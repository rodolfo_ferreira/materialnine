package com.dolphs.materialblank;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class TablesManager extends SQLiteOpenHelper {

    public static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "NINTHIT";
    public static final String TABLE_CONTACTS = "CONTACTS";
    public static final String TABLE_NUMBERS = "NUMBERS";
    private static TablesManager instance;

    private TablesManager(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public static TablesManager getInstance(Context context) {
        if (instance == null)
            instance = new TablesManager(context);
        return instance;
    }

    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + TABLE_CONTACTS + "("
                + ContactsRepository.COLUMN_ID + " INTEGER PRIMARY KEY, "
                + ContactsRepository.COLUMN_NAME + " VARCHAR(255)NOT NULL);");

        db.execSQL("CREATE TABLE " + TABLE_NUMBERS + "("
                + ContactsRepository.COLUMN_ID + " INTEGER NOT NULL, "
                + ContactsRepository.COLUMN_NEW_NUMBER + " VARCHAR(255) DEFAULT NULL, "
                + ContactsRepository.COLUMN_IS_SELECTED + " VARCHAR(255)NOT NULL, "
                + ContactsRepository.COLUMN_HB_CHANGED + " VARCHAR(255)NOT NULL, "
                + ContactsRepository.COLUMN_NUMBER + " VARCHAR(255) UNIQUE NOT NULL, "
                + "FOREIGN KEY(" + ContactsRepository.COLUMN_ID + ") REFERENCES " + TABLE_CONTACTS + "(" + ContactsRepository.COLUMN_ID + "));");
    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // This database is only a cache for online data, so its upgrade policy
        // is
        // to simply to discard the data and start over
        db.execSQL("DROP TABLE " + TABLE_CONTACTS);
        db.execSQL("DROP TABLE" + TABLE_NUMBERS);
        onCreate(db);
    }

    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }
}
