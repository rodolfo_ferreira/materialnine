package com.dolphs.materialblank;

import android.app.ProgressDialog;
import android.content.ContentProviderOperation;
import android.content.Context;
import android.content.OperationApplicationException;
import android.os.AsyncTask;
import android.os.RemoteException;
import android.provider.ContactsContract;

import java.util.ArrayList;

/**
 * Created by Rodolfo on 02/06/2015.
 */
public class ContentUpdate extends AsyncTask<String, Context, Void> {
    private ProgressDialog mProgressDialog;
    private Context mContext;
    private boolean show;

    public ContentUpdate(Context context) {
        this.mContext = context;
        this.show = true;
        mProgressDialog = new ProgressDialog(context);
        mProgressDialog.setCancelable(false);
        mProgressDialog.setMessage(mContext.getString(R.string.updating));
        mProgressDialog.setTitle(mContext.getString(R.string.progress_title));
        mProgressDialog.setIndeterminate(true);
    }

    @Override
    protected Void doInBackground(String... params) {
        ContactsRepository repository = ContactsRepository.getInstance(mContext);
        final MainActivity myActivty = (MainActivity) mContext;
        ContactsManager contactsManager = ContactsManager.getInstance();
        ArrayList<Contact> contacts = contactsManager.getContacts();
        for (int i = 0; i < contacts.size(); i++) {
            Contact contact = contacts.get(i);
            if (contact.isSelected()) {
                try {
                    repository.add(contact);
                } catch (Exception e) {

                }
                editContact(contact);
            }
        }
        contactsManager.buildContacts(mContext);
        myActivty.onFragmentInteraction();

        return null;
    }

    @Override
    protected void onPreExecute() {
        mProgressDialog.show();
    }

    @Override
    protected void onPostExecute(Void result) {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }

    public boolean editContact(Contact contact) {
        boolean result = true;

        ArrayList<ContentProviderOperation> ops = new ArrayList<ContentProviderOperation>();

        if (contact.isSelected() && contact.getNewNumber() != null) {

            // Name
            ContentProviderOperation.Builder builder = ContentProviderOperation.newUpdate(ContactsContract.Data.CONTENT_URI);
            builder.withSelection(ContactsContract.Data.CONTACT_ID + "=?" + " AND " +
                    ContactsContract.CommonDataKinds.Phone.NUMBER + "=?" + " AND " +
                    ContactsContract.Data.MIMETYPE + "=?", new String[]{String.valueOf(contact.getId()), contact.getNumber(),
                    ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE});
            builder.withValue(ContactsContract.CommonDataKinds.Phone.NUMBER, contact.getNewNumber());
            ops.add(builder.build());
            try {
                mContext.getContentResolver().applyBatch(ContactsContract.AUTHORITY, ops);
            } catch (OperationApplicationException e) {
                result = false;
            } catch (RemoteException e) {
                result = false;
            }
        }
        return result;
    }

    public interface onFinish {

        public void onFinish();
    }
}
