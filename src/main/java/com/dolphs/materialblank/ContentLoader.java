package com.dolphs.materialblank;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.os.AsyncTask;
import android.provider.ContactsContract;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import com.dolphs.materialdesign.FloatingActionButton;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/**
 * Created by Rodolfo on 02/06/2015.
 */
public class ContentLoader extends AsyncTask<String, Context, Void> {
    private ProgressDialog mProgressDialog;
    private Context mContext;
    private boolean show;
    private ArrayList<Contact> mContacts;

    public ContentLoader(Context context) {
        this.mContext = context;
        this.show = true;
        mProgressDialog = new ProgressDialog(context);
        mProgressDialog.setCancelable(false);
        mProgressDialog.setMessage(mContext.getString(R.string.progress_message));
        mProgressDialog.setTitle(mContext.getString(R.string.progress_title));
        mProgressDialog.setIndeterminate(true);
    }

    @Override
    protected Void doInBackground(String... params) {
        ContactsManager contactsManager = ContactsManager.getInstance();
        contactsManager.buildContacts(mContext);
        this.mContacts = contactsManager.getContacts();
        final TextView mTextView1 = (TextView) ((Activity) mContext).findViewById(R.id.welcome_textview);
        final TextView mTextView2 = (TextView) ((Activity) mContext).findViewById(R.id.contacts_number);
        final TextView mTextView3 = (TextView) ((Activity) mContext).findViewById(R.id.contacts_avaible);
        final FloatingActionButton mFab = (FloatingActionButton) ((Activity) mContext).findViewById(R.id.fab_welcome);

        String format = ((Activity) mContext).getResources().getString(R.string.contacts_number_format);
        int size = mContacts.size();
        final String text1 = String.format(format, size);
        final MainActivity myActivty = (MainActivity) mContext;

        int avaibleNumber = 0;
        for (int i = 0; i < size; i++) {
            Contact c = mContacts.get(i);
            if (c.getNewNumber() != null)
                avaibleNumber++;
        }

        format = ((Activity) mContext).getResources().getString(R.string.contacts_avaible_format);
        final String text2 = String.format(format, avaibleNumber);

        myActivty.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mTextView2.setText(text1);
                mTextView3.setText(text2);
                animate(mTextView1, R.anim.fall_in, View.VISIBLE);
                animate(mTextView2, R.anim.fall_in, View.VISIBLE);
                animate(mTextView3, R.anim.fall_in, View.VISIBLE);
                animate(mFab, R.anim.zoom_in, View.VISIBLE);
            }
        });


        return null;
    }

    @Override
    protected void onPreExecute() {
        mProgressDialog.show();
    }

    @Override
    protected void onPostExecute(Void result) {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }

    public void animate(final View view, int animatonResId, final int visibility) {
        final Animation anim = AnimationUtils.loadAnimation(mContext, animatonResId);
        anim.setDuration(1000);
        view.post(new Runnable() {
            @Override
            public void run() {
                view.setAnimation(anim);
                view.startAnimation(anim);
                view.setVisibility(visibility);
            }
        });


    }

    public ArrayList<Contact> getContacts() {
        return mContacts;
    }
}
