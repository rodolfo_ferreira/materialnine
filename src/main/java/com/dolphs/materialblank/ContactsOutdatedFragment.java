package com.dolphs.materialblank;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ContactsOutdatedFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ContactsOutdatedFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ContactsOutdatedFragment extends Fragment implements ContactsFragment.OnPageScrolled {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private RecyclerView mRecyclerView;
    private RecyclerView.LayoutManager mLayoutManager;
    private ContactsAdapter mAdapter;
    private ArrayList<Contact> mContacts;
    private Toolbar mToolbar;
    private OnFragmentInteractionListener mListener;

    public ContactsOutdatedFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ContactsOutdatedFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ContactsOutdatedFragment newInstance(String param1, String param2) {
        ContactsOutdatedFragment fragment = new ContactsOutdatedFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        mContacts = new ArrayList<Contact>();
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_outdated_contacts, container, false);
        ContactsManager contactsManager = ContactsManager.getInstance();
        ArrayList<Contact> contacts = contactsManager.getContacts();
        for (int i = 0; i < contacts.size(); i++) {
            Contact contact = contacts.get(i);
            if (contact.getNewNumber() != null)
                mContacts.add(contact);
        }
        mAdapter = new ContactsAdapter(mContacts, getActivity());
        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.outdated_recycler_view);
        mLayoutManager = new LinearLayoutManager(getActivity());
        mLayoutManager.scrollToPosition(0);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setAdapter(mAdapter);
        return rootView;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void OnPageScrolled(final int position, Context context) {
        if (position == 1) {
            MainActivity activity = ((MainActivity) context);
            mToolbar = (Toolbar) activity.findViewById(R.id.contacts_toolbar);
            mRecyclerView = (RecyclerView) activity.findViewById(R.id.outdated_recycler_view);
            mAdapter = (ContactsAdapter) mRecyclerView.getAdapter();
            mAdapter.notifyDataSetChanged();
            MyOnMenuItemClickListener menuItemClickListener = new MyOnMenuItemClickListener(context, mAdapter);
            mToolbar.setOnMenuItemClickListener(menuItemClickListener);
            mAdapter.notifyDataSetChanged();

        }
    }


    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }

}
